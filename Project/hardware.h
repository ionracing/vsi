/*
* Hardware mapping files. Contains pin/port functions and configuration settings.
* Uses SPL librarys from STM for definitions.
*/

#ifndef HARDWARE_H
#define HARDWARE_H

#if defined COMPILING_FOR_DEVBOARD
    #warning "compiling for development board!"
    #include "hardware_discovery.h"
#elif defined COMPILING_FOR_V1
    #include "hardwareV1.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
