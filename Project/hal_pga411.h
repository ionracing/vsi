/*
* Module containing all the hardware specific code for the PGA411.
* note - hardware dependent.
*/

#ifndef _HAL_PGA411_H
#define _HAL_PGA411_H

/* Dependencies --------------------------------------------------------*/
#include "hal_gpio.h" /* GPIO functions */
#include "stdint.h"
#include "stm32f4xx_spi.h"
#include "hardware.h" /* Pin and port mapping, common for all GPIOs*/

/* Macros --------------------------------------------------------------*/
#define HAL_VA0_LOW IO_LOW(PGA411_pxx[va0])
#define HAL_VA0_HIGH IO_HIGH(PGA411_pxx[va0])

#define HAL_VA1_LOW IO_LOW(PGA411_pxx[va1])
#define HAL_VA1_HIGH IO_HIGH(PGA411_pxx[va1])

#define HAL_INHB_LOW IO_LOW(PGA411_pxx[inhb])
#define HAL_INHB_HIGH IO_HIGH(PGA411_pxx[inhb])

#define HAL_PGA_FAULTRES_LOW IO_LOW(PGA411_pxx[faultres])
#define HAL_PGA_FAULTRES_HIGZ IO_HIGH(PGA411_pxx[faultres])

#define HAL_PGA_RESET_N_LOW IO_LOW(PGA411_pxx[nRes])
#define HAL_PGA_RESET_N_HIGH IO_HIGH(PGA411_pxx[nRes])

#define HAL_IS_ORDCLK_HIGH (IO_READ_BIT(PGA411_pxx[ordCLK]))

/* Public functions ----------------------------------------------------*/

/* GPIO functions */

/**
* @brief    Reads the paralell interface of the PGA411.
* @input    none.
* @output   uint16_t - returns the paralell interface data. 12 bits.
*/
uint16_t hal_pga411ReadORD(void);

/**
* @brief    Initializes all the hardware pins for the PGA411.
* @note     hardware.h contains structures with the pin and
*           port mappings used by this function.
* @input    none.
* @output   none.
*/
void hal_pga411initGPIO(void);

/* SPI functions */

/**
* @brief    Initializes the SPI module with the settings used for 
*           the PGA411.
* @note     Hardware.h contains the pin and port mapping structure for
*           the SPI.
*/

void hal_PGAspiInit(void);
/**
* @brief    For transmitting a frame consisting of 4 bytes.
* @input    frame: the frame to be sent.
* @output   uint16_t : Return frame from the SPI.
*/
uint16_t hal_PGAspiXmit4B(uint16_t frame);

#endif
