#ifndef PARKCLARKE_H
#define PARKCLARKE_H

/* Dependencies -----------------------------------------------------*/
#include "stdint.h"
#include "sinLUT.h"
/* public functions -------------------------------------------------*/
/**
* @brief	Performs the forward park and clarke transformations.
*			Takes the three phase currents (ia, ib) and the current
*			angle theta and calculates the rotating frame values 
*			(iq and id).
* @note		Assumes a balanced three phase load (ia + ib + ic = 0).
* @input	theta - The current angle. A value between 0->4095
*			corresponding to 0->360 degrees.
* @note		Uses a 12-bit look up table for the sin and cosine 
*			functions.
* @input	ia - the current in phase A.
* @input	ib - the current in phase B.
* @output	iq - pointer to the output for the calculated q-axis current.
* @output	id - pointer to the output for the calculated d-axis current.
*/
void inline parkClarke_forward(
	uint16_t theta, 
	float ia, 
	float ib, 
	float* iq, 
	float* id
);
	
/**
* @brief	performs the inverse clarke and park transforms.
*			Takes the rotating frame values (vq, vd and the 
*			angle theta) and calculates the three phase output
*			(va, vb and vc).
* @input	theta - The current angle. A value between 0->4095
*			corresponding to 0->360 degrees.
* @note		Uses a 12-bit look up table for the sin and cosine 
*			functions.
* @input	vq - the q-axis voltage.
* @input	vd - the d-axis voltage.
* @output	va - pointer to the output variable va.
* @output	vb - pointer to the output variable vb.
* @output   vc - pointer to the output variable vc.	
*/
void inline parkClarke_inverse(
	uint16_t theta,
	float vq,
	float vd, 
	float* va,
	float* vb, 
	float* vc
);
	
#endif
	