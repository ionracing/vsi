#ifndef PGA_411_H
#define PGA_411_H
/* Dependencies -----------------------------*/
#include "stdint.h"
/* Must be initialized before using the module */
#include "hal_delay.h" 
#include "hal_pga411.h"

/* Public variables/constants ---------------*/

/*
* @brief : Initializes the pga411 to be ready for configuration.
*/
void pga411_init(void); 

/*
* @brief configures the pga411 to operate in normal condition:
* parallel interface, acceleration mode.
*/
void pga411_defaultConfig(void);

/**
* @brief    Polls the angle from the chip. 
*           PGA411 writes a new angle every 100ns. The function starts a
*           new conversion and busy waits for the data. Expect > 100ns delay
*           from this function.
* @output   float : angle in degrees. A number between 0->360.
*/
float pga411_getAngle(void);

/**
* @brief    Polls the angle from the chip. 
*           PGA411 writes a new angle every 100ns. The function starts a
*           new conversion and busy waits for the data. Expect > 100ns delay
*           from this function.
* @output   uint16_t : raw angle, a value between 0 -> 4095 corresponding to 
*           a angle of 0->360 degrees.
*/
uint16_t pga411_getAngleRaw(void);

/*
* @brief : To continue operation if a fault has been detected
* it needs to be cleared. Use this function to clear the faults.
*/

void pga411_faultReset(void);
#endif
