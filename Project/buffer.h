/*
* Circular buffer for reading CAN messages.
*/

#ifndef BUFFER_H
#define BUFFER_H

/* Dependencies ---------------------------------------------------------- */
#include "stdint.h"
#include "stm32f4xx_can.h" /* For the canTxMsg structure. */

/* Enums ------------------------------------------------------------------*/
typedef enum 
{
    bufferFull,
    bufferEmpty,
    bufferNoError
} buffer_error_t;

/* Typedefs ---------------------------------------------------------------*/
/* 
* The type of element to be stored. Can be a struct or just a int. 
* TODO : write the code so that the type of element is a input parameter.
*/
typedef uint16_t tElement;

/* Local variables --------------------------------------------------------*/
typedef struct 
{
    tElement *buf;  /* Block of memory*/
    uint16_t size;  /* must be a power of two */
    uint16_t read;  /* Holds current read position: 0 to (size-1)  */
    uint16_t write; /* Hods current write position: 0 to (size-1) */
}circBuffer_t;

/* Sets the variables of the bufferStruct */
void buffer_init(circBuffer_t* cb, tElement* buffer, uint16_t length);
/*wrapped subtraction to determine the length of available data */
uint16_t buffer_LengthData(circBuffer_t *cb);
buffer_error_t buffer_write(circBuffer_t *cb, tElement data);
buffer_error_t buffer_read(circBuffer_t *cb, tElement *data);
buffer_error_t buffer_free(circBuffer_t *cb);

#endif
