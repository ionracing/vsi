#ifndef __SINLUT_H
#define __SINLUT_H
/*-------------------- Dependencies --------------------------------*/
#include <stdint.h>

/*-------------------- Constants -----------------------------------*/
#define nbrOfPoints 4096
#define nintyDeg 1024 /* (90/360) * 4096 */

/*-------------------- Macros --------------------------------------*/
#define LUT_SINE(angle) (LUT_sin[ WRAP_ANGLE(angle) ])
#define LUT_COSINE(angle) (LUT_sin[ ( WRAP_ANGLE(angle + nintyDeg) ) ]) 

/* If the angle is larger than 4095 (360 degrees) it wraps to around.
* It skips zero when wrapping around (360 = 0).
* 4097 -> 1, 4098 ->2.. 4096 + 1024 = 1024.
*/
#define WRAP_ANGLE(angle)  (angle > nbrOfPoints ? angle-nbrOfPoints : angle)

/*-------------------- SINUS LUT -----------------------------------*/
extern const float LUT_sin[nbrOfPoints];


#endif
