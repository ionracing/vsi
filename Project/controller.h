/*
* Controller code for the VSI.
* Interrupt driven at 15kHz.
*/

#ifndef CONTROLLER_H
#define CONTROLLER_H

/* Dependencies --------------------------------------------------------------------*/
#include "stdint.h"
#include "HAL_PWM.h"
#include "parkClarke.h"
#include "hal_gpio.h"

/* Typedefs ------------------------------------------------------------------------*/
typedef struct
{
	uint16_t freq; /* Interrupt frequency in kHz */
	uint16_t motorTempThr;
	uint16_t igbtTempThr;
	uint16_t maxRPM;
	uint16_t maxCurrent; /* Max min current on the output */
	uint16_t maxVoltage;
	uint16_t minVoltage;
}ctrlConfig_t;

/* Function declerations -----------------------------------------------------------*/

/*
* @brief:	initializes the controller. Call controller enable to start
*			the PWM and interrupt function.
* input:	confic : struct containing the controller configuration.
* output:   uint8_t : 1 - succesfull, 0 - failure.
*/
uint8_t controller_init(ctrlConfig_t config);

/*
* @brief:	Enables the PWM output and interrupt function of the controller.
*/
void controller_enable(void);
/*
* @brief:	Disables the PWM output of the controller.
*/
void controller_disable(void);

uint16_t getMaxCurrent(void);


#endif
