#include "parkClarke.h"

/* Constants ---------------------------------------------------------------------*/
const float sqrt3Inv = 0.577350269189626; /* (1/sqrt(3))*/
const float sqrt3 = 1.732050807568877;

void inline parkClarke_forward(
	uint16_t theta, 
	float ia, 
	float ib, 
	float* iq, 
	float* id
){
	/* Clarke transformation */
	float ibeta;
	/* ialpha = ia; */
	ibeta = sqrt3Inv * (ia + 2*ibeta);
	
	/* Park transformation */
	*id = ia * LUT_COSINE(theta) + ibeta * LUT_SINE(theta);
	*iq = ibeta * LUT_COSINE(theta) - ia * LUT_SINE(theta);
}

void inline parkClarke_inverse(
	uint16_t theta,
	float vq,
	float vd, 
	float* va,
	float* vb, 
	float* vc
){
	float vAlpha, vBeta;
	
	/* Inverse Park transformation */
	vAlpha = vd * LUT_COSINE(theta) - vq*LUT_SINE(theta);
	vBeta = vq * LUT_COSINE(theta) + vd*LUT_SINE(theta);
	
	/* Inverse clarke transformation */
	*va = vAlpha;
	*vb = (-vAlpha + sqrt3 * vBeta )/2;
	*vc = (-vAlpha - sqrt3 * vBeta) /2;
}
