#include "hal_analog.h"


/* Private constants ------------------------------------------------*/
#define ADC_SAMPLE_TIME ADC_SampleTime_3Cycles
#define ADC_CCR_ADDRESS 0x40012308

/* Current sensor constants */
const float VREF_CURRENTS = 1.65; /* Ideal zero current value */
#define CURRENT_CONVERSION_GAIN 100/3 /* used for calculating current*/

/* Private variables ------------------------------------------------*/
/* Array containing the sampled values */
__IO uint16_t aADCDualConvertedValue[NBR_OF_ANALOG_INPUPTS];

/* Current sensor variables */
/* Used for current calculation and offset correction */
float vAdcCurrentUZero = 1.65; 
float vAdcCurrentVZero = 1.65; 

/* Private function declerations ------------------------------------*/

/**
* @brief    Enables the DMA2 peripheral clock.
*           Configures DMA2 stream0 channel0. Used for transferring the 
*           read values to the array "aADCDualConvertedValue".
*/
static void DMA_config(void);

/**
* @brief    Enables the GPIO peripheral clocks used as analog inputs.
*           Configures the ADC input GPIO's as analog inputs.
*/
static void gpio_config(void);

/**
* @brief    Enables the ADC1 & ADC2 peripheral clocks. Configures the
*           the regular channel for the analog inputs. (12bit, scan
*           conversion mode). Groups which channels that is sampled
*           together:
*           ADC1    : | IU | Tu | Tw  | Tamb   |
*           ADC2    : | IV | Tv | Vdc | Tmotor |
*/
static void adc_regularChannelConfig(void);

/**
* @brief    Configures the common settings for the ADC's.
*           (simultaneous sampling, DMA access).
*/
static void adc_commonConfig(void);


/* Public functions -------------------------------------------------*/
void hal_analogInit(void)
{   
    /*DMA2 stream0 channel0 configuration */
    DMA_config();
    /* Configure GPIO's as analog inputs */
    gpio_config();
    /* Configure ADC common settings */
    adc_commonConfig();
    /* ADC1 & ADC2 regular channel configuration */
    adc_regularChannelConfig();
    /* Enable DMA request after last treansfer (Multi-adc mode) */
    ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
    /* Enable ADC1 and ADC2 */
	ADC_Cmd(ADC1, ENABLE);
    ADC_Cmd(ADC2, ENABLE);
}


void hal_analogCurrentsRead(float* iU, float* iV)
{
    *iU = ((float)aADCDualConvertedValue[0] - vAdcCurrentUZero) * CURRENT_CONVERSION_GAIN;
    *iV = ((float)aADCDualConvertedValue[1] - vAdcCurrentUZero) * CURRENT_CONVERSION_GAIN;
}

void hal_analogCurrentsCalibrate(void)
{   
    /*TODO: Start conversion, wait for completion */
    
    /* Set zero point value for current calculation */
    vAdcCurrentUZero = (float)aADCDualConvertedValue[0];
    vAdcCurrentVZero = (float)aADCDualConvertedValue[0];
}

void hal_analogCurrentsGetOffsetValues(float *iUOffset, float *iVOffset)
{
    *iUOffset = vAdcCurrentUZero - VREF_CURRENTS;
    *iVOffset = vAdcCurrentVZero - VREF_CURRENTS;
}

void hal_analogCurrentsResetCalibration(void)
{
    /* Set the zero value to the ideal value */
    vAdcCurrentUZero = VREF_CURRENTS;
    vAdcCurrentVZero = VREF_CURRENTS;
}

/* Private functions ------------------------------------------------*/
static void DMA_config(void)
{
    /* Enable DMA peripheral clock */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
    /* Configure DMA */
    DMA_InitTypeDef DMA_InitStructure;

    DMA_InitStructure.DMA_Channel = DMA_Channel_0; 
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&aADCDualConvertedValue;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC_CCR_ADDRESS;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = NBR_OF_ANALOG_INPUPTS;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;         
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA2_Stream0, &DMA_InitStructure);

    /* DMA2_Stream0 enable */
    DMA_Cmd(DMA2_Stream0, ENABLE);
}

static void gpio_config(void)
{
    GPIO_InitTypeDef GPIO_initStruct;
    GPIO_StructInit(&GPIO_initStruct);
    /* ADC pins configuration */
    for(uint8_t k = 0; k < NBR_OF_ANALOG_INPUPTS; k++)
    {
        /* Enable gpio clock */
        HAL_gpioEnableClock(analog_pxx[k].port);
        
        /* Configure pin and port to ADC input */
        GPIO_initStruct.GPIO_Mode  = GPIO_Mode_AN; /*Analog*/
        GPIO_initStruct.GPIO_Pin = analog_pxx[k].pin;
        GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
        GPIO_Init(analog_pxx[k].port, &GPIO_initStruct);
    }
}

static void adc_regularChannelConfig(void)
{
    /* Enable ADC peripheral clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
    
    ADC_InitTypeDef ADC_initStruct;
    ADC_StructInit(&ADC_initStruct);
    
    /* Configure the ADC Prescaler, conversion resolution and data 
        alignment. Same settings used for ADC1 and ADC2 */
	ADC_initStruct.ADC_Resolution = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode = ENABLE; /* Convert all channels */
	ADC_initStruct.ADC_ContinuousConvMode = DISABLE; 
	ADC_initStruct.ADC_ExternalTrigConvEdge = 0;
	ADC_initStruct.ADC_ExternalTrigConv = 0;
	ADC_initStruct.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion = 4; /* Four conversions per channel */
	/* Configure ADC 1 */
    ADC_Init(ADC1, &ADC_initStruct);
    /* Configure ADC2 */
	ADC_Init(ADC2, &ADC_initStruct);
    
    /* Configure all the adc inputs as regular channels:
    */
    ADC_RegularChannelConfig(ADC1, hw_analogChannelMapping[currentU], 1, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC1, hw_analogChannelMapping[tempU], 2, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC1, hw_analogChannelMapping[tempW], 3, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC1, hw_analogChannelMapping[tempAmb], 4, ADC_SAMPLE_TIME);

    ADC_RegularChannelConfig(ADC2, hw_analogChannelMapping[currentV], 1, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC2, hw_analogChannelMapping[tempV], 2, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC2, hw_analogChannelMapping[vdc], 3, ADC_SAMPLE_TIME);
    ADC_RegularChannelConfig(ADC2, hw_analogChannelMapping[tempMotor], 4, ADC_SAMPLE_TIME);
}

static void adc_commonConfig(void)
{
    ADC_CommonInitTypeDef ADC_commonInitStruct;
	ADC_CommonStructInit(&ADC_commonInitStruct);
    
	ADC_commonInitStruct.ADC_Mode = ADC_DualMode_RegSimult;
	ADC_commonInitStruct.ADC_Prescaler = ADC_Prescaler_Div2;
    ADC_commonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);
}