#ifndef __TRANSFORM_H
#define __TRANSFORM_H
/*-------------------- Dependencies --------------------------------*/
#include <stdint.h>

/*-------------------- Types ---------------------------------------*/

/*-------------------- Functions -----------------------------------*/
/*
* Converts three phase reference frame to two-axis orthogonal 
* Stationary frame.
* Inputs:
*   iA - current reading from phase A.
*   iA - current reading from phase B. 
* retval: 
*   iAlpha - 
*   iBeta - 
*/
void transform_clarke(float ia, float ib, float *ialpha, float *ibeta);
void transform_clarkeInverse(float *ia, float *ib, float *ic, float ialpha, float ibeta);

/* The park transformation rotates the two-phase orthogonal currents 
*   i-alpha and i-beta with theta degrees. Common convenetion is to 
*   use iq for the transformed Ialpha current and id for the transformed
*   Ibeta current. Here iAlpa and iBeta have been used for both.
*
* Inputs:
*   theta - must be a value between 0->4096, which corresponds
*               to a angle from 0->360.
*   ialpha - Inputs the un-transformed current, returns the 
*               transformed current (iq).
*   ibeta - Inputs the un-transformed current, returns the 
*               transformed current (id).
*   
*/
void transform_park(uint16_t theta, float *ialpha, float *ibeta);
void transform_parkInverse(uint16_t theta, float *ialpha, float *ibeta);
void test(void);
#endif