#ifndef BSP_GPIO_H
#define BSP_GPIO_H


/* Dependencies -------------------------------------------- */
#include "stm32f4xx_gpio.h"
#include "hardware.h"
/* MACROS ---------------------------------------------------*/

/*Constants -------------------------------------------------*/
/* Used for setting the GPIO high or low */
typedef enum
{
    io_on,
    io_off,
}gpioState_t;

/*Public functions ------------------------------------------*/
void BSP_GPIOinitDigIn(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void BSP_gpioEnableClock(GPIO_TypeDef* GPIOx);
void BSP_GPIOinitDigOut(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void BSP_GPIO_setOutput(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, gpioState_t state);

/** @brief : Initializes the GPIO as a alternate function.
* @Note: for using the pins you have to configure them as 
* as a alternate function using the function GPIO_PinAFConfig().
* @input GPIOx : The GPIO port to be initialized.
* @input GPIO_Pin : The GPIO Pin to be initialized.
* @retval : None
*/
void BSP_GPIO_initAF(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint8_t BSP_GPIO_pin2pinSource(uint16_t gpioPin);
#endif

