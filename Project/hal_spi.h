#ifndef _HAL_SPI_H
#define _HAL_SPI_H

/* Dependencies --------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx_spi.h"

/* Typededs ------------------------------------------------------------*/

/* Struct containing the SPI configuration */
typedef struct
{
}hal_spiConfig_t;

/* Public functions ----------------------------------------------------*/

/**
* @brief    Initializes the SPI module.
* @input    config : contains the SPI configuration.
*/
void hal_spiInit(hal_spiConfig_t config);

/**
* @brief    For transmitting a frame consisting of 4 bytes.
* @input    frame: the frame to be sent.
* @output   uint16_t : Return frame from the SPI.
*/
uint16_t hal_spiXmit4B(uint16_t frame);

#endif