#include "hardware.h"

const portPin_t led[]=
{
[led1] = {GPIOE, GPIO_Pin_14},
[led2] = {GPIOC, GPIO_Pin_6},
[led3] = {GPIOC, GPIO_Pin_7},
[led4] = {GPIOE, GPIO_Pin_12},
};

const portPin_t PGA411_pxx[] =
{
    [ord0] = {GPIOD, GPIO_Pin_5},
    [ord1] = {GPIOD, GPIO_Pin_6},
    [ord2] = {GPIOD, GPIO_Pin_7},
    [ord3] = {GPIOB, GPIO_Pin_5},
    [ord4] = {GPIOB, GPIO_Pin_8},
    [ord5] = {GPIOE, GPIO_Pin_0},
    [ord6] = {GPIOE, GPIO_Pin_1},
    [ord7] = {GPIOC, GPIO_Pin_13},
    [ord8] = {GPIOC, GPIO_Pin_14},
    [ord9] = {GPIOC, GPIO_Pin_3},
    [ord10] = {GPIOC, GPIO_Pin_2},
    [ord11] = {GPIOC, GPIO_Pin_1},
    [ord12] = {GPIOC, GPIO_Pin_0},
    [prd] = {GPIOD, GPIO_Pin_4},
    [va0] = {GPIOC, GPIO_Pin_8},
    [va1] = {GPIOA, GPIO_Pin_12},
    [amode] = {GPIOD, GPIO_Pin_1},
    [inhb] = {GPIOD, GPIO_Pin_3},
    [faultres] = {GPIOB, GPIO_Pin_12},
    [nRes] = {GPIOD, GPIO_Pin_2},
    [pgaCs] = {GPIOD, GPIO_Pin_15},
    [pgaSclk] = {GPIOC, GPIO_Pin_10},
    [pgaSdi] = {GPIOC, GPIO_Pin_12},
    [pgaSdo] = {GPIOC, GPIO_Pin_11},
};


const portPin_t analog_pxx[NBR_OF_ANALOG_INPUPTS] =
{
    [currentU] = {GPIOA, GPIO_Pin_6},
    [currentV] = {GPIOA, GPIO_Pin_5},
    [tempU] = {GPIOC, GPIO_Pin_5},
    [tempV] = {GPIOC, GPIO_Pin_4},
    [tempW] = {GPIOA, GPIO_Pin_7},
    [vdc] = {GPIOB, GPIO_Pin_0},
    [tempAmb] = {GPIOA, GPIO_Pin_3},
    [tempMotor] = {GPIOA, GPIO_Pin_4},
};

const uint8_t hw_analogChannelMapping[NBR_OF_ANALOG_INPUPTS] =
{
    [currentU] = ADC_Channel_0,
    [currentV] = ADC_Channel_0,
    [tempU] = ADC_Channel_0,
    [tempV] = ADC_Channel_0,
    [tempW] = ADC_Channel_0,
    [vdc] = ADC_Channel_0,
    [tempAmb] = ADC_Channel_0,
    [tempMotor] = ADC_Channel_0,
};
    