/* TODO:	calculate period time values
* 			Calculate dead time values
*			Write Overvoltage shutdown function
*/

#include "HAL_PWM.h"
/* Private typedefs --------------------------------------------------------------------*/

/* Private constants -------------------------------------------------------------------*/

#define deadtime 0x0A

const uint32_t periodList[5] = 
{
	5000,	/*  5kHz,*/
	5000,	/*	8kHz */
	5000,	/*  10kHz */
	5000,	/*	15Khz */
	5000,	/*	20kHz */
};

/*Private variables --------------------------------------------------------------------*/
uint8_t isPwm6ChannelConfigured = 0;
uint32_t period;
PWM_state_t irqState_f = disable;
PWM_state_t overvoltageProtection_f = disable;
uint16_t maxDr = 90; /* Maximum duty ratio in percentage */
uint16_t minDr = 10; /* Minimum duty ratio in percentage */

void HAL_pwm6channelInitTim1(const portPin_t portPin[6], const PWM_state_t irqState, PWM_freq_t freq)
{	
    /* PWM Init */
    GPIO_InitTypeDef GPIO_initStruct;
    TIM_TimeBaseInitTypeDef TIM_baseInitStruct;
    TIM_OCInitTypeDef TIM_ocInitStruct;
    TIM_BDTRInitTypeDef TIM_bdtrInitStruct;
    
    /*Enable clocks*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
    
    /* Configure GPIO */
    /* Alternate function mapping */
	for(int k = 0; k<6; k++)
	{
		/* Enable GPIO Clock */
		HAL_gpioEnableClock(portPin[k].port);
		/* Configure Alternate function for the gpio */
		GPIO_PinAFConfig(portPin[k].port, HAL_gpioPin2pinSource(portPin[k].pin),GPIO_AF_TIM1);
		/* Configure GPIO's as alternate function pins */
		GPIO_initStruct.GPIO_Mode 	= GPIO_Mode_AF;
		GPIO_initStruct.GPIO_OType 	= GPIO_OType_PP;
		GPIO_initStruct.GPIO_Pin 	= portPin[k].pin;
		GPIO_initStruct.GPIO_PuPd 	= GPIO_PuPd_NOPULL;
		GPIO_initStruct.GPIO_Speed 	= GPIO_Speed_100MHz;
		GPIO_Init(portPin[k].port, &GPIO_initStruct);
	}
	/*Calculate the period for given frequency */
	period = periodList[freq];
    /* Timer 1 configuration for PWM */
    TIM_baseInitStruct.TIM_Period 		 = period;
    TIM_baseInitStruct.TIM_Prescaler 	 = 0;
    TIM_baseInitStruct.TIM_CounterMode 	 = TIM_CounterMode_CenterAligned1;
    TIM_baseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM1, &TIM_baseInitStruct);
    TIM_Cmd(TIM1, ENABLE);
    
    /* PWM Config */
    TIM_ocInitStruct.TIM_OCMode 		= TIM_OCMode_PWM1;
    TIM_ocInitStruct.TIM_OCPolarity 	= TIM_OCPolarity_High;
    TIM_ocInitStruct.TIM_OutputNState	= TIM_OutputNState_Enable;
    TIM_ocInitStruct.TIM_OutputState 	= TIM_OutputState_Enable;
    TIM_ocInitStruct.TIM_OCNPolarity 	= TIM_OCNPolarity_High;
    TIM_ocInitStruct.TIM_Pulse = 0;
    TIM_OC1Init(TIM1, &TIM_ocInitStruct); 
    TIM_OC2Init(TIM1, &TIM_ocInitStruct);
	TIM_OC3Init(TIM1, &TIM_ocInitStruct);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);
    
	/* Configure the break functionality */
    TIM_bdtrInitStruct.TIM_OSSRState 		= TIM_OSSRState_Disable;
    TIM_bdtrInitStruct.TIM_OSSIState 		= TIM_OSSIState_Enable;
    TIM_bdtrInitStruct.TIM_LOCKLevel 		= TIM_LOCKLevel_OFF;
    TIM_bdtrInitStruct.TIM_DeadTime	 		= deadtime;
    TIM_bdtrInitStruct.TIM_Break			= TIM_Break_Enable;
    TIM_bdtrInitStruct.TIM_BreakPolarity 	= TIM_BreakPolarity_High;
    TIM_bdtrInitStruct.TIM_AutomaticOutput 	= TIM_AutomaticOutput_Disable;
    TIM_BDTRConfig(TIM1, &TIM_bdtrInitStruct);
    
    TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_OCMode_PWM1);
    TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_OCMode_PWM1);
    TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_OCMode_PWM1); 		
	
	/* Enable the three PWM outputs*/
	TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable);
	TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable);
	TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable);
	/* Enable the three complementary PWM outputs*/
	TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable);
	TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);
	TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable);
	
	TIM_SetCompare4(TIM1, (uint32_t)(TIM1->ARR));
	/* Configure interrupt on channel 4 */
	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
	nvicStructure.NVIC_IRQChannelSubPriority = 1;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);
	/* Enable the interrupt event */
	/* Set the configuration done flag */
	isPwm6ChannelConfigured = 1;
}


void HAL_PWM6channel_enableOutput(void)
{
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_ITConfig(TIM1, TIM_IT_CC4, ENABLE);
}


void HAL_PWM6channel_disableOutput(void)
{
	TIM_CtrlPWMOutputs(TIM1, DISABLE);
	TIM_ITConfig(TIM1, TIM_IT_CC4, DISABLE);
}

void HAL_PWM6channel_OvervoltageProtection(PWM_state_t state)
{
	if(state == enable)
	{
		overvoltageProtection_f = enable;
		TIM1->CCR1 = (period * 0)/100;
		TIM1->CCR2 = (period * 0)/100;
		TIM1->CCR3 = (period * 0)/100;
	}else if(state == disable)
	{
		overvoltageProtection_f = disable;
	}
}

void PWM_updateRefCh1(uint8_t dr)
{
	if(overvoltageProtection_f == disable)
	{	
		if(dr > period){dr = maxDr;}
//		else if(dr < 0){period = 0;}
		TIM1->CCR1 = (dr);
	}
}


void PWM_updateRefCh2(uint8_t dr)
{
	if(overvoltageProtection_f == disable)
	{	
		if(dr > period){dr = maxDr;}
//		else if(dr < 0){period = 0;}
		TIM1->CCR2 = (dr);
	}
}


void PWM_updateRefCh3(uint8_t dr)
{
	if(overvoltageProtection_f == disable)
	{	
		if(dr > period){dr = maxDr;}
//		else if(dr < 0){period = 0;}
		TIM1->CCR3 = (dr);
	}
}

/*Private functions -----------------------------------------------------------------------------*/


