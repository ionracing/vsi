/*
* Just some extra functions for simplifying usage of the
* stm32f4xx_gpio.h abstraction layer from the STM
* standard peripheral library.
*/

#ifndef HAL_GPIO_H
#define HAL_GPIO_H
/* Dependencies --------------------------------------------------------------------*/
#include "stm32f4xx_gpio.h"

/* Typedefs ------------------------------------------------------------------------*/

/*	Used for mapping ports and pins together. 
	Not currently used by any functions in this module
	since SPL does not map them together, but added here since 
	it's still useful for modules dependent on this module.
*/
typedef struct
{
	GPIO_TypeDef* port;
	uint16_t pin;
}portPin_t;

/* Macros --------------------------------------------------------------------------*/
#define IO_HIGH(portPin)(portPin.port->BSRRL = portPin.pin)
#define IO_LOW(portPin)(portPin.port->BSRRH = portPin.pin)
#define IO_READ_BIT(portPin) (portPin.port->IDR & portPin.pin)

/* Functions -----------------------------------------------------------------------*/
/* 
* @brief: Enables the clock for a specific PORT.
* input : GPIO_TypeDef*, Port.
* output : none.
*/
void HAL_gpioEnableClock(GPIO_TypeDef* GPIOx);

/*
* @brief :	Translates the gpio pin number to the pinsource number
* 			used by STM's Standard Peripheral library.
*/
uint8_t HAL_gpioPin2pinSource(uint16_t gpioPin);

#endif
