#include "transform.h"
/* ----------------------------------- Dependencies ---------------------------------------- */
#include "sinLUT.h"
/* ----------------------------------- Private prototypes ---------------------------------- */

/* ----------------------------------- Constants ------------------------------------------- */
const float sqrt3Inv = 0.577350269189626; /* (1/sqrt(3))*/
const float sqrt3 = 1.732050807568877;
/* ----------------------------------- Public functions ------------------------------------ */
void transform_clarke(float ia, float ib, float *ialpha, float *ibeta)
{
    *ialpha = ia;
    *ibeta = sqrt3Inv*(ia + 2*ib);
}

void transform_clarkeInverse(float *ia, float *ib, float *ic, float ialpha, float ibeta)
{
    float tmp = sqrt3 * ibeta; 
    *ia = ialpha;
    *ib = (-ialpha + tmp)/2;
    *ic = (-ialpha - tmp)/2;
}

void transform_park(uint16_t theta, float *ialpha, float *ibeta)
{
   // *ialpha = *ialpha*LUT_COSINE(theta) + *ibeta * LUT_SINE(theta);
   // *ibeta = *ibeta * LUT_COSINE(theta) - *ialpha * LUT_SINE(theta);
}

void transform_parkInverse(uint16_t theta, float *ialpha, float *ibeta)
{
    *ialpha = *ialpha*LUT_COSINE(theta) - *ibeta * LUT_SINE(theta);
    *ibeta = *ibeta * LUT_COSINE(theta) + *ialpha * LUT_SINE(theta);   
}
void test(void)
{
    
}
/* ----------------------------------- Private functions ----------------------------------- */