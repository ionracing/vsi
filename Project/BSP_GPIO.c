#include "BSP_GPIO.h"

void BSP_GPIOinitDigOut(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    BSP_gpioEnableClock(GPIOx);
    
    GPIO_InitTypeDef GPIO_initStruct;
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
    GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = GPIO_Pin; 
    GPIO_Init(GPIOx, &GPIO_initStruct);
}

void BSP_GPIOinitDigIn(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    BSP_gpioEnableClock(GPIOx);
    
    GPIO_InitTypeDef GPIO_initStruct;
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_IN;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = GPIO_Pin; 
    GPIO_Init(GPIOx, &GPIO_initStruct);
}

void BSP_gpioEnableClock(GPIO_TypeDef* GPIOx)
{
    if(GPIOx == GPIOA)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    }else if(GPIOx == GPIOB)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    }else if(GPIOx == GPIOC)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    }else if(GPIOx == GPIOD)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    }else if(GPIOx == GPIOE)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    }else if(GPIOx == GPIOF)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
    }
}


void BSP_GPIO_initAF(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    BSP_gpioEnableClock(GPIOx);
    
    GPIO_InitTypeDef GPIO_initStruct;
    /* Set default values to the init struct */
    GPIO_StructInit(&GPIO_initStruct);
    
    /* Configure GPIO as alternate function */
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_AF;
    GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = GPIO_Pin; 
    GPIO_Init(GPIOx, &GPIO_initStruct);
}
uint16_t gpio_pins[16] = {1,2,4,8,16,32,64, 128, 256,512,1024,2048,4096,8192,16384,32768}; 

uint8_t BSP_GPIO_pin2pinSource(uint16_t gpioPin)
{
    for(uint8_t k = 0; k<16; k++)
    {
        if(gpio_pins[k] == gpioPin)
        {
            return k;
        }
    }
    return 16; /* GPIO_pins does not go as higher than 15. */
}
