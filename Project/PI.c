/*
* Series connected PI regulator with limited output and dynamic integrator
* anti-windup implementation.
*/

#include "pi.h"

/* Macros ----------------------------------------------------------------*/
#define MAX(val1,val2) (val1 > val2 ? val1 : val2)
#define MIN(val1, val2) (val1 < val2 ? val1 : val2)

/* Public functions ------------------------------------------------------*/
void PI_update(PIregulator_t *PI)
{
	float iMax, iMin; /* min and max values for integrator anti-windup */

	PI->error = PI->setPoint - PI->plantValue;
	PI->proportionalTerm = PI->error * PI->Ka; 
	PI->integralSum += PI->Kb * PI->proportionalTerm;
	
	/* Integral Anti windup */
	iMax = MAX(PI->outputMax - PI->proportionalTerm, 0);
	iMin = MIN(PI->outputMin - PI->proportionalTerm, 0);
	if(PI->integralSum > iMax){ PI->integralSum = iMax;}
	else if(PI->integralSum < iMin){ PI->integralSum = iMin;}

	/* Calculate and limit output */
	PI->output = PI->proportionalTerm + PI->integralSum;
	if(PI->output > PI->outputMax){PI->output = PI->outputMax;}
	else if(PI->output < PI->outputMin){PI->output = PI->outputMin;}
}
