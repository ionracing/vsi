#ifndef PI_H
#define PI_H
/* Series connected PI regulator using floating point operation */

/*  Dependencies ---------------------------------------------------------*/
#include "stdint.h"

/* Typedefs --------------------------------------------------------------*/
typedef struct 
{
	float setPoint; /* Desired set point */
	float plantValue; /* The current value */
	float error; /* Error term */
	float integralSum; /* Proportional term * Kb */
	float proportionalTerm; /* Error * proportional gain */
	float Ka; /* Proportional gain */
	float Kb; /* Integral gain */
	float outputMax; /* Max output of the regulator */
	float outputMin; /* Minimum output of the regulator */
	float output; /* The output of the regulator */
}PIregulator_t;

/* Public functions ------------------------------------------------------*/

/**
* @brief	Updates the PI-regulator variables.
* @note		Series connected PI regulator.
* @note		Uses dynamic integral anti windup method. 
*			H = max(H-P,0) and L = min(L-P, 0). If the output is satured
*			the integrator cannot do anything, so it is limited based upon
*			how large the proportional output is  (P).
*/
void PI_update(PIregulator_t *PI);
#endif
