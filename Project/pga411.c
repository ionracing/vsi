/* 
* For communication with the PGA411 chip.
* Based upon http://www.ti.com/lit/an/slaa708/slaa708.pdf 
* TODO: Write HAL code.
*/
#include "pga411.h"

/* Private constants ----------------------------------------*/
/* Register constants */
/* Constants used for calculating the angle*/
#define RES_ANGLE 4096 /* 2^12 */
/* Constants used for calculating velocity */
#define RES_VEL 33554432 /* 2^25 */
#define FCLK 20000000 /* Clock frequency typ 20Mhz */
/* SPI Dummy constant for reading data */
#define SPI_DUMMY 0x10

/* Private enums --------------------------------------------*/
enum pga411_states{DIAG, NORM}; /* Selects pga411 state */
enum {READ, WRITE}; /* Selects SPI operation */

/* REGISTER LOCATIONS 
* See PGA411 datasheet for register field descriptions.
*/
enum regLocation
{
	DEV_OVUV1, /* Diag state only */
	DEV_OVUV2, /* Diag state only */
	DEV_OVUV3, /* Diag state only */
	DEV_OVUV4, /* Diag state only */
	DEV_OVUV5, /* Diag state only */
	DEV_OVUV6, /* Diag state only */
	DEV_TLOOP_CFG, /* Diag state only */
	DEV_AFE_CFG, /* Diag state only */
	DEV_PHASE_CFG, /* Diag state only */
	DEV_CONFIG1, /* Diag state only */
	DEV_CONTROL1, /* Diag state only */
	DEV_CONTROL2, /* Diag state only */
	DEV_CONTROL3, /* All states */
	DEV_STAT1, /* Read only */
	DEV_STAT2, /* Read only */
	DEV_STAT3, /* Read only */
	DEV_STAT4, /* Read only */
	DEV_STAT5, /* Read only */
	DEV_STAT6, /* Read only */
	DEV_STAT7, /* Read only */
	DEV_CLCRC, /* Diag state only */
	DEV_CRC, /* Diag state only */
	CRCCALC, /* Read only */
	DEV_EE_CTRL1, /* Diag state only */
	DEV_CRC_CTRL1, /* Diag state only */
	DEV_EE_CTRL4, /* Diag state only */
	DEV_UNLK_CTRL1, /* Diag state only */
};
/* Structures -------------------------------------------------*/

/* SPI frame struct. Union trick allows the same struct to be used for
* for both outgoing and incoming frames.
*/
typedef union spiFrame
{
	/* Outgoing data frame */
	struct 
	{
		uint32_t mcrc:6; /* Master's CRC for data, bits 0..5*/
		uint32_t reserved:2; /* Reserved, always 0, bits 6..7*/
		uint32_t dataout:16; /* data, MSB first, bits 8..23*/
		uint32_t addr:8; /* Register adress, bits 24..31*/
	}outMsg;
	/* Incoming messages */
	struct
	{
		uint32_t scrc:6; /* Slave's CRC for data, bits 0..5*/
		uint32_t stat:2; /* status of SPI communication, bits 6..7*/
		uint32_t datain:16; /* data, MSB first, bits 8..23 */
		uint32_t ecbk:8; /* register adress, bits 24..31 */
	}inMsg;
	/* The whole frame */
	struct 
	{
		uint32_t frame;
	}wholeFrame;

}pga411_spi_frame_t;

/* Structure for spir write adress, read adress and default value.
 * Use the regLocation enum for accessing the structure.
 */
typedef struct 
{
	const uint16_t read_add;
	const uint16_t write_add;
	const uint16_t def_val;
	uint16_t real_val; /* Realtime register content*/
}PGA411_reg_t;

/* Register structure init */
#define N_REGISTERS 27
PGA411_reg_t PGA411_regs[N_REGISTERS] =
{
	{0x53, 0x87, 0x8B40, 0}, /* DEV_OVUV1 */
	{0x68, 0x26, 0x00ED, 0}, /* DEV_OVUV2 */
	{0x65, 0x17, 0xFCFF, 0}, /* DEV_OVUV3 */
	{0xEC, 0x39, 0x07E2, 0}, /* DEV_OVUV4 */
	{0x52, 0x75, 0x1C00, 0}, /* DEV_OVUV5 */
	{0xE9, 0x83, 0x038F, 0}, /* DEV_OVUV6 */
	{0xA6, 0x42, 0x0514, 0}, /* DEV_TLOOP_CFG */
	{0xC2, 0x91, 0x0005, 0}, /*DEV_AFE_CFG*/
	{0x57, 0x85, 0x1400, 0}, /*DEV_PHASE_CFG*/
	{0xBE,0xEB,0x0002, 0}, /*DEV_CONFIG1*/
	{0x90,0x0D,0x0000, 0}, /*DEV_CONTROL1*/
	{0x63,0x38,0x0000, 0}, /*DEV_CONTROL2*/
	{0xDD,0xAE,0x0000, 0}, /*DEV_CONTROL3*/
	{0x81,0,0x0000, 0}, /*DEV_STAT1*/
	{0x4D,0,0x0000, 0}, /*DEV_STAT2*/
	{0x84,0,0x0000, 0}, /*DEV_STAT3*/
	{0x1F,0,0x0000, 0}, /*DEV_STAT4*/
	{0x41,0,0x0000, 0}, /*DEV_STAT5*/
	{0x64,0,0x0000, 0}, /*DEV_STAT6*/
	{0xE1,0,0x0000, 0}, /*DEV_STAT7*/
	{0x4F,0xFC,0x00CE, 0}, /*DEV_CLCRC*/
	{0x0F,0xE7,0x0000, 0}, /*DEV_CRC*/
	{0xD9,0,0x00FF, 0}, /*CRCCALC*/
	{0xE3,0x6E,0x0000, 0}, /*DEV_EE_CTRL1*/
	{0x7A,0xB6,0x0000, 0}, /*DEV_CRC_CTRL1*/
	{0xBA,0x56,0x0000, 0}, /*DEV_EE_CTRL4*/
	{0x64,0x95,0x0000, 0}, /*DEV_UNLK_CTRL1*/
};

/* Private function prototypes ------------------------------*/
void pga411_reset(void);
void pga411_state(enum pga411_states states);
void pga411_deviceUnclock(void);
/* TODO: Write the function and the return frame definition */
pga411_spi_frame_t pga411_XmitSPI(uint16_t dir, uint16_t reg, uint16_t wdata);
void pga411_writeReg(uint16_t reg, uint16_t data);
uint16_t pga411_readReg(uint16_t reg);
uint16_t pga411_crc2 (uint32_t datain);

/* Public functions -----------------------------------------*/
void pga411_init(void)
{
	//gpio_pga411Init();
	//spi_init();
	pga411_reset();
} 

void pga411_defaultConfig(void)
{
	int i;

	pga411_state(DIAG); 
	pga411_deviceUnclock(); 
	for(i = 0; i < 12; i++)
	{
		pga411_XmitSPI(WRITE, i, PGA411_regs[i].def_val);
	}
	pga411_state(NORM);
}


float pga411_getAngle(void)
{
	float angle;
    uint16_t ORDx;
    ORDx = pga411_getAngleRaw();
	angle = 360 * ORDx / RES_ANGLE;
	return angle;
}

/*TODO : CRC CHECK */
uint16_t pga411_getAngleRaw(void)
{
    uint16_t ORDx;
	/* ORD[11:0] should be set to hiz */
	/* Set VA0 low */
	HAL_VA0_LOW;
	/* Set VA1 high */
	HAL_VA1_HIGH;
	/* Set inhb high, PGA will then start to update the angle*/
	HAL_INHB_HIGH;
	/* Wait for ORDCLK to go high */
    while(!HAL_IS_ORDCLK_HIGH);
	/* Read ORD[11:0] */
    ORDx = hal_pga411ReadORD();
    /* Disable output again */
	HAL_INHB_LOW;
    /* Return raw angle */
    return ORDx;
}

void pga411_faultReset()
{
//	HAL_PGA_FAULTRES_LOW; /* assert FAULTRES low - open collector */
//	HAL_DELAY_US(1000); /* Hold the state for 1ms */
//	HAL_PGA_FAULTRES_HIGZ; /* Return to high impedance state */
}


/* Private functions ----------------------------------------*/

void pga411_reset(void)
{
	HAL_PGA_RESET_N_LOW; /* RESET_N low */
//	HAL_DELAY_US(500); /* hold it for 500us */
	HAL_PGA_RESET_N_HIGH; /* Release from reset */
//	HAL_DELAY_US(10000); /* Wait for atleast 10 ms */
}

/* Change the state of the of pga411 diagnostic/normal*/
void pga411_state(enum pga411_states state)
{
	uint16_t temp;
	if(state == DIAG) /* Enter diagnostic state */
	{
		temp = pga411_readReg(DEV_CONTROL3);
		temp |= 0x0004; /* Set bit SPIDIAG */
		pga411_writeReg(DEV_CONTROL3, temp);
	}
	else /* Return to normal state */
	{
		temp = pga411_readReg(DEV_CONTROL1);
		temp |= 0x0001; /*Set bit diagexit */
		pga411_writeReg(DEV_CONTROL1, temp);  
	}
}

/*
* @brief : Unclocks the pga411. Must be in diagnostic state
* to unlcok it.
*/
void pga411_deviceUnclock(void)
{
	pga411_writeReg(DEV_UNLK_CTRL1, 0x000F);
	pga411_writeReg(DEV_UNLK_CTRL1, 0x0055);
	pga411_writeReg(DEV_UNLK_CTRL1, 0x00AA);
	pga411_writeReg(DEV_UNLK_CTRL1, 0x00F0);
}

/*
* brief: spi write wrapper for writing data to pga411 registers.
*/
void pga411_writeReg(uint16_t reg, uint16_t data)
{
	pga411_XmitSPI(WRITE, reg, data);
}

/*
* brief: spi read wrapper for reading data from pga411 registers.
*/
uint16_t pga411_readReg(uint16_t reg)
{
	/* First read returns garbage */
	pga411_XmitSPI(READ, reg, SPI_DUMMY);
	/* Second read returns requested data */
	return (pga411_XmitSPI(READ, reg, SPI_DUMMY).inMsg.datain);
}
/*
* SPI transmit/recieve function.
*/
pga411_spi_frame_t pga411_XmitSPI(uint16_t dir, uint16_t reg, uint16_t wdata)
{
	pga411_spi_frame_t out, in;
	/* Read data? */
	if(dir == READ) out.outMsg.addr = PGA411_regs[reg].read_add;
	/* if not the write data */
	else out.outMsg.addr = PGA411_regs[reg].write_add;
	/* Compose the rest of the frame */
	out.outMsg.dataout = wdata;
	out.outMsg.reserved = 0x00; /* Always zero */
	out.outMsg.mcrc = pga411_crc2(out.wholeFrame.frame); /* Calculate Tx crc*/
//	in.frame = hal_spiXmit4B(out.frame);

	/* Check RX CRC */
	if(pga411_crc2(in.wholeFrame.frame) != in.inMsg.scrc)
	{
//		hal_assert(); /* if error -> terminate */
	}
	return(in);
}

/*CRC6 calculation */
/*TODO:
	define CRC_INITSEED, CRC_BYTECOUNT.
*/
uint16_t pga411_crc2 (uint32_t datain)
{
//	uint16_t byte_idx, bit_idx, crc = (CRC_INITSEED << 2);

//	/* Byte for byte starting from MSB (3-2-1) */
//	for(byte_idx = CRC_BYTECOUNT; byte_idx >= 1; byte_idx--)
//	{
//		/* XOR-in new byte from left to right */
//		crc ^= ((datain << (byte_idx<<3)) & 0x000000FF);
//		/* bit by bit each byte */
//		for(bit_idx = 0; bit_idx < 8; bit_idx++)
//		{
//			crc = crc << 1 ^ ( crc & 0x80 ? (CRC_POLYNOM << 2) : 0 );
//		}
//	}
//	return (crc >> 2 & CRC_INITSEED); /* Restore two bit offset */
return 0;
}

