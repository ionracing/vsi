/*
* Module description:
* Module contains all code for reading the sensor values.
* includes ADC initialization, conversion functions and calibration functions.
*/

#ifndef HAL_ANALOG_H
#define HAL_ANALOG_H

/* Dependencies -----------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx_adc.h"
#include "hal_gpio.h"
#include "hardware.h"
#include "stm32f4xx_dma.h"

void hal_analogInit(void);

/**
* @brief    Starts the conversion of all the sensors. 
*           TODO: Write code.
*/
void hal_analogStartConversion(void);

void hal_analogReadTemps(float* tmpU, float* tmpV, float* tmpW); /* Not done*/
void hal_analogReadVoltage(float* vdc); /* Not done*/

/* Current sensor functions -----------------------------------------*/
void hal_analogCurrentsRead(float* iU, float* iV);

/**
* @brief    Calibrates the zero point offzet of the sensors. 
* @note     Use hal_analogCurrentsGetOffsetValues() to get the
*           calculated offset values.
* @note     Make sure zero current is going through the sensors 
*           when using this function.
*/
void hal_analogCurrentsCalibrateZeroPoint(void);

/**
* @brief    Returns the offset values that are currently in use.
*/
void hal_analogCurrentsGetOffsetValues(float *iUOffset, float *iVOffset);

/**
* @brief    Resets any calibration values used for current conversion.
*/
void hal_analogCurrentsResetCalibration(void);

/**
* @brief    returns the raw ADC values for the currents. 
*/
void hal_analogCurrentsGetRawValues(float* iU, float* iV); /* Not done*/

#endif
