#include "HAL_PWM.h"

/* Private constants -------------------------------------------------------------------*/

portPin_t defPins[]
{
}

/*Private variables --------------------------------------------------------------------*/
uint8_t isPwm6ChannelConfigured = 0;

void HAL_pwm6channelInitTim1(PWM_pinSet_t pinSet, PWM_irqState_t irqState, uint16_t freq)
{
    /* PWM Init */
    GPIO_InitTypeDef GPIO_initStruct;
    TIM_TimeBaseInitTypeDef TIM_baseInitStruct;
    TIM_OCInitTypeDef TIM_ocInitStruct;
    TIM_BDTRInitTypeDef TIM_bdtrInitStruct;
    
    SysTick_Config(SystemCoreClock/1000); /*Systick - 1ms */
    
    /*Enable clocks*/
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    
    /* Configure GPIO */
    /* Alternate function mapping */
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource8, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource11, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource13, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_TIM1);
    
    GPIO_initStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_initStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_13;
    GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOE, &GPIO_initStruct);
    GPIO_initStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_Init(GPIOB, &GPIO_initStruct);
    
    /* Timer 1 configuration for PWM */
    TIM_baseInitStruct.TIM_Period = PWM_PERIOD;
    TIM_baseInitStruct.TIM_Prescaler = 0;
    TIM_baseInitStruct.TIM_CounterMode = TIM_CounterMode_CenterAligned1;
    TIM_baseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM1, &TIM_baseInitStruct);
    TIM_Cmd(TIM1, ENABLE);
    
    /* PWM Config */
    TIM_ocInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_ocInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_ocInitStruct.TIM_OutputNState = TIM_OutputNState_Enable;
    TIM_ocInitStruct.TIM_OutputState = TIM_OutputState_Enable;
    TIM_ocInitStruct.TIM_OCNPolarity = TIM_OCNPolarity_High;
    TIM_ocInitStruct.TIM_Pulse = 0;
    TIM_OC1Init(TIM1, &TIM_ocInitStruct);
    TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
    
    TIM_bdtrInitStruct.TIM_OSSRState = TIM_OSSRState_Enable;
    TIM_bdtrInitStruct.TIM_OSSIState = TIM_OSSIState_Enable;
    TIM_bdtrInitStruct.TIM_LOCKLevel = TIM_LOCKLevel_OFF;
    TIM_bdtrInitStruct.TIM_DeadTime = 0xFA;
    TIM_bdtrInitStruct.TIM_Break = TIM_Break_Disable;
    TIM_bdtrInitStruct.TIM_BreakPolarity = TIM_BreakPolarity_Low;
    TIM_bdtrInitStruct.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;
    TIM_BDTRConfig(TIM1, &TIM_bdtrInitStruct);
    
    TIM_SelectOCxM(TIM1, TIM_Channel_1, TIM_OCMode_PWM1);
    TIM_SelectOCxM(TIM1, TIM_Channel_2, TIM_OCMode_PWM1);
    TIM_SelectOCxM(TIM1, TIM_Channel_3, TIM_OCMode_PWM1);
    
    TIM_CCxCmd(TIM1, TIM_Channel_1, TIM_CCx_Enable);
    TIM_CCxCmd(TIM1, TIM_Channel_2, TIM_CCx_Enable);
    TIM_CCxCmd(TIM1, TIM_Channel_3, TIM_CCx_Enable);
    
    TIM_CCxNCmd(TIM1, TIM_Channel_1, TIM_CCxN_Enable);
    TIM_CCxNCmd(TIM1, TIM_Channel_2, TIM_CCxN_Enable);
    TIM_CCxNCmd(TIM1, TIM_Channel_3, TIM_CCxN_Enable);
    
    TIM_CtrlPWMOutputs(TIM1, ENABLE);
}


void HAL_PWM6channel_enableOutput(void)
{
	if(isPwm6ChannelConfigured == 1)
	{
		/* Enable the output */
	}/* Else do nothing */
}

void HAL_PWM6channel_disableOutput(void)
{
	if(isPwm6ChannelConfigured == 1)
	{
		/* Enable the output */
	}/* Else do nothing */
}