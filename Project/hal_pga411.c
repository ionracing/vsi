#include "hal_pga411.h"
/* Macros --------------------------------------------------------------*/

/* Typedefs ------------------------------------------------------------*/
typedef union ordMapping 
{
    struct
    {
        uint16_t ord0:1;
        uint16_t ord1:1;
        uint16_t ord2:1;
        uint16_t ord3:1;
        uint16_t ord4:1;
        uint16_t ord5:1;
        uint16_t ord6:1;
        uint16_t ord7:1;
        uint16_t ord8:1;
        uint16_t ord9:1;
        uint16_t ord10:1;
        uint16_t ord11:1;
    }bit;
    
    struct
    {
        uint16_t ord;
    }val;
}ord_t;


ord_t ordStruct;

/* Public functions ----------------------------------------------------*/
uint16_t hal_pga411ReadORD(void)
{
    ordStruct.bit.ord0  = IO_READ_BIT(PGA411_pxx[ord0]);
    ordStruct.bit.ord1  = IO_READ_BIT(PGA411_pxx[ord1]);
    ordStruct.bit.ord2  = IO_READ_BIT(PGA411_pxx[ord2]);
    ordStruct.bit.ord3  = IO_READ_BIT(PGA411_pxx[ord3]);
    ordStruct.bit.ord4  = IO_READ_BIT(PGA411_pxx[ord4]);
    ordStruct.bit.ord5  = IO_READ_BIT(PGA411_pxx[ord5]);
    ordStruct.bit.ord6  = IO_READ_BIT(PGA411_pxx[ord6]);
    ordStruct.bit.ord7  = IO_READ_BIT(PGA411_pxx[ord7]);
    ordStruct.bit.ord8  = IO_READ_BIT(PGA411_pxx[ord8]);
    ordStruct.bit.ord9  = IO_READ_BIT(PGA411_pxx[ord9]);
    ordStruct.bit.ord10 = IO_READ_BIT(PGA411_pxx[ord10]);
    ordStruct.bit.ord11 = IO_READ_BIT(PGA411_pxx[ord11]);
    return ordStruct.val.ord; 
}


void hal_pga411initGPIO(void)
{
    const uint16_t nbrOfInputs = 15;
    const uint16_t nbrOfOutputsOD = 6;
    
    portPin_t inputs[nbrOfInputs] = 
    {
        PGA411_pxx[ord0],
        PGA411_pxx[ord1],
        PGA411_pxx[ord2],
        PGA411_pxx[ord3],
        PGA411_pxx[ord4],
        PGA411_pxx[ord5],
        PGA411_pxx[ord6],
        PGA411_pxx[ord7],
        PGA411_pxx[ord8],
        PGA411_pxx[ord9],
        PGA411_pxx[ord10],
        PGA411_pxx[ord11],
        PGA411_pxx[ordCLK],
        PGA411_pxx[prd],
    };
    /*TODO : should any of the ouputs be configured as push pull ? */
    portPin_t outputsOD[nbrOfOutputsOD] =
    {
        PGA411_pxx[va0],
        PGA411_pxx[va1],
        PGA411_pxx[amode],
        PGA411_pxx[inhb],
        PGA411_pxx[faultres],
        PGA411_pxx[nRes],
    };
    
    /* Configure inputs */
    for(int k = 0; k<nbrOfInputs; k++)
    {
        HAL_gpioEnableClock(inputs[k].port);
        GPIO_InitTypeDef GPIO_initStruct;
        GPIO_initStruct.GPIO_Mode   = GPIO_Mode_IN;
        GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL; /*hiz*/
        GPIO_initStruct.GPIO_OType  = GPIO_OType_OD;
        GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
        GPIO_initStruct.GPIO_Pin    = inputs[k].pin; 
        GPIO_Init(inputs[k].port, &GPIO_initStruct);
    }
    
    /* Configure ouputs */
        for(int k = 0; k<nbrOfOutputsOD; k++)
    {
        HAL_gpioEnableClock(outputsOD[k].port);
        GPIO_InitTypeDef GPIO_initStruct;
        GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
        GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL; /* should there be a pull-up/down? */
        GPIO_initStruct.GPIO_OType  = GPIO_OType_OD;
        GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
        GPIO_initStruct.GPIO_Pin    = outputsOD[k].pin; 
        GPIO_Init(outputsOD[k].port, &GPIO_initStruct);
    }
}

/* TODO: finish the PGA init function - see code for BMS */
void hal_PGAspiInit(void)
{
//    SPI_InitTypeDef SPI_initStruct;
}

/* TODO: write the code :) */
uint16_t hal_PGAspiXmit4B(uint16_t frame)
{
    return 0;
}
