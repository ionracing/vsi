#ifndef HAL_PWM_H
#define HAL_PWM_H
/* Dependencies ---------------------------------------------------------------*/
#include "hal_gpio.h"
#include "stm32f4xx_rcc.h"

/* Macros ---------------------------------------------------------------------*/

/* Typedefs ------------------------------------------------------------------*/
typedef enum
{
	def,
	alt,
}PWM_pinSet_t;

typedef enum
{
	enable,
	disable,
}PWM_state_t;

typedef enum
{
	freq5kHz,
	freq8kHz,
	freq10kHz,
	freq15kHz,
	freq20kHz,
}PWM_freq_t;
#define IS_PWM_FREQ_T(freq) (freq == freq5kHz || freq == freq8kHz || freq == freq10kHz || freq == freq15kHz || freq == freq20kHz)

/*Structure containing the configuration for the six channel PWM */
typedef struct
{
	portPin_t portPin[6]; /* Pins used for PWM */
	uint16_t maxDutyRatio; /* Max Duty ratio, number between 0->100*/
	uint16_t minDutyRatio; /* Min duty Ration number between 0->100*/
	PWM_state_t irqState; /* Wether to configure a IRQ on ch4 or not*/
	PWM_freq_t freq; /* The PWM frequency*/
	uint16_t deadtime; /* Deadtime insertion value, number of ticks */
}pwm6ChannelConfig_t;

/* Public functions ------------------------------------------------------------*/
/**
* @brief:	initializes three PWM outputs and their complementary output for TIM1.
* 			the PWM module is hardware controlled. Use macros PWM_UPDATE_REF_CHx 
*			to change the PWM reference. 
* @note:	Uses center aligned PWM.
* @note:	The APB2 TIM1 clock should have a frequency of -- for the frequency settings
*			to be correct.
* @input: 	pins - struct array containing the pins and ports used. Refer to the 
*			STM32f407 datasheet for compatible gpio to be used with TIM1.
*			alternative pinSet : A = , nA = , B= , nB = , C = , nC = 
* @input:	irqState : on - enables the a IRQ for the midpoint of the timer.
*			off - disables the IRQ.
* @input:	freq - Sets the PWM frequency.
*/
void hal_pwm6channelInitTim1(const portPin_t portPin[6], const PWM_state_t irqState, PWM_freq_t freq);

/**
* @brief:	Enables the PWM output (and IRQ if configured). 
*			HAL_pwm6channelInitTim1 must be called before this function.
*/
void hal_pwm6channel_enableOutput(void);

/**
* @brief:	Disables the PWM output (and IRQ if configured).
*			HAL_pwm6channelInitTim1 must be called before this function.
*/
void hal_pwm6channel_disableOutput(void);
/**
* @brief	Called if overvoltage is detected. Turns all top
* 			transistor off and all bottom transistors. This 
*			prevents voltage build up in the capacitor bank.
* @note		Implements deadtime to not get shot through in phase legs.
* @input	state : enable if overvoltage is detected.
*				  : disable if overvoltage is not detected. 
* @note		if overvoltage protection have been enabled 
*			PWM_updateRefChx function are ignored. Overvoltage
*			protection needs to be disabled to continue operation.
*/
void hal_pwm6channel_OvervoltageProtection(PWM_state_t state);
/**
* @brief:	Updates the reference signal for channel 1.
* @input:	dr - duty ratio, number between 0->100. Min and Max
*			values are programmed by init function. 		
*/
void hal_pwmUpdateRefCh1(uint8_t dr);

/**
* @brief:	Updates the reference signal for channel 2.
* @input:	dr - duty ratio, number between 0->100. Min and Max
*			values are programmed by init function. 		
*/
void hal_pwmUpdateRefCh2(uint8_t dr);

/**
* @brief:	Updates the reference signal for channel 3.
* @input:	dr - duty ratio, number between 0->100. Min and Max
*			values are programmed by init function. 		
*/
void hal_pwmUpdateRefCh3(uint8_t dr);

#endif
