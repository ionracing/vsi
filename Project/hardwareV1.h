#ifndef HARDWAREV1_H
#define HARDWAREV1_H

#include "hal_gpio.h"

/* LEDs */
enum
{
	led1,
	led2,
	led3,
	led4,
};
extern const portPin_t led[]; /*Pin and port mapping*/

/* PGA411 CHIP */
enum
{
    ord0, /* ORD 0-> 11 parallel interface */
    ord1,
    ord2,
    ord3,
    ord4,
    ord5,
    ord6,
    ord7,
    ord8,
    ord9,
    ord10,
    ord11,
    ord12, /* unused */
    ordCLK, /* parallel interface clock */
    prd, /* Parallel interface parity check bit */
    va0, /* Configuration bit */
    va1, /* Configuration bit */
         /*Va0 = 0 & Va1 = 1 -> angle output 
           Va0 = 1 & va1 = 0 -> velocity output */
    amode, /* Accleration mode on/off [1/0]*/
    inhb, /* 1 -> new update, 0-> keep last output */
    faultres, /* HIGH -> LOW -> HIGH -> reset faults in PGA */
    nRes, /* To set the whole chip in reset */
    pgaCs, /* SPI chip select */
    pgaSclk, /* SPI clock */
    pgaSdi, /* SPI, slave data in */
    pgaSdo, /* SPI, Slave data out */
};
extern const portPin_t PGA411_pxx[];


/* Analog inputs ----------------------------------------------------*/
#define NBR_OF_ANALOG_INPUPTS 8
enum
{
    currentU,
    currentV,
    tempU,
    tempV,
    tempW,
    vdc,
    tempAmb,
    tempMotor,
};
extern const portPin_t analog_pxx[NBR_OF_ANALOG_INPUPTS];
const uint8_t hw_analogChannelMapping[NBR_OF_ANALOG_INPUPTS];

/* CAN BUS ------------------------------------------*/
#define PIN_CAN_TX GPIO_Pin_9
#define PORT_CAN_TX GPIOB
#define PIN_CAN_RX GPIO_Pin_0
#define PORT_CAN_RX GPIOD
/* Inputs -------------------------------------------*/
/* Supply status */
#define PIN_5V_ERROR GPIO_Pin_1
#define PORT_5V_ERROR GPIOA
#define PIN_3V3A_ERROR GPIO_Pin_13
#define PORT_3V3A_ERROR GPIOE
#define PIN_3V3_ERROR GPIO_Pin_0
#define PORT_3V3_ERROR GPIOA
/* Current overload detected */
#define PIN_OC GPIO_Pin_1
#define PORT_OC GPIOB
/* Pilotline present */
#define PIN_PL_PR GPIO_Pin_11
#define PORT_PL_PR GPIOA
/* Run signal from ECU */
#define PIN_RUN GPIO_Pin_2
#define PORT_RUN GPIOA
/* outputs ------------------------------------------*/
/* Pilotline enable/disable */
#define PIN_PL_CTRL GPIO_Pin_9
#define PORT_PL_CTRL GPIOC
/* Driver ---------------------------------------------*/
#define PIN_NTRIP GPIO_Pin_11
#define PORT_NTRIP GPIOD
/* PWM */
#define CH_PWM TIM1
#define PIN_PWM_U GPIO_Pin_8
#define PORT_PWM_U GPIOA
#define PIN_NPWM_U GPIO_Pin_13
#define PORT_NPWM_U GPIOB

#define PIN_PWM_V GPIO_Pin_9
#define PORT_PWM_V GPIOA
#define PIN_NPWM_V GPIO_Pin_14
#define PORT_NPWM_V GPIOB

#define PIN_PWM_W GPIO_Pin_10
#define PORT_PWM_W GPIOA
#define PIN_NPWM_W GPIO_Pin_15
#define PORT_NPWM_W GPIOB
/* EEPROM ----------------------------------------------*/
#define PIN_EEP_WP GPIO_Pin_15
#define PORT_EEP_WP GPIOE
#define PIN_EEP_SCL GPIO_Pin_10
#define PORT_EEP_SCL GPIOB
#define PIN_EEP_SDA GPIO_Pin_11
#define PORT_EEP_SDA GPIOB



#endif
