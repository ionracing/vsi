#include "hal_delay.h"
/* Private variables --------------------------------------------*/
volatile uint32_t ticks = 0;

/* Public functions ---------------------------------------------*/
void hal_delay_systickInit(uint16_t frequency)
{
	SysTick_Config(SystemCoreClock / frequency);
    NVIC_SetPriority(SysTick_IRQn, 0);
}

uint32_t hal_delay_getMillis(void)
{
    return ticks;
}

void hal_delay_ms (uint32_t t)
{
    uint32_t startTest;
    uint32_t endTest;
    startTest = hal_delay_getMillis();
    endTest = startTest + t;
    while(startTest < endTest)
    {
        startTest = hal_delay_getMillis();
    }
}

/*Interrupt handler ----------------------------------------------*/
void SysTick_Handler(void)
{
    ticks++;
}
