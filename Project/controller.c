#include "controller.h"

/* Private function declerations ---------------------------------------------------*/

/*Private Typedefs -----------------------------------------------------------------*/
typedef union
{
	float iq;
	float id;
	float tempMotor;
	float tempIgbtA;
	float tempIgbtB; /* */
	float dcLinkVoltage; /* DC LINK VOLTAGE */
}controllerVar_t;

/* Private variables ---------------------------------------------------------------*/
ctrlConfig_t configVar; /* Configurations variables, min/max values */
controllerVar_t ctrlVar; /* Variables used for the controller */


/* Public functions ----------------------------------------------------------------*/
uint8_t controller_init(ctrlConfig_t config)
{
	return 1;
}
/* Private Functions ---------------------------------------------------------------*/

/* Background task */
void TIM1_CC_IRQHandler()
{
	static volatile int k = 10;
	if (TIM_GetITStatus(TIM1, TIM_IT_CC4) != RESET)
		{
			TIM_ClearITPendingBit(TIM1, TIM_IT_CC4);
			
			/* TODO : Start Conversion of readings (currents, voltage, temperatures) */
			/* TODO : Read Angle */
			/* TODO : Wait for current conversion complete */
			/* Park Clarke transformation */
			/* TODO: PI-calculations*/
			/* Inverse transformation */
			/* TODO: Scale output */
			/* Update PWM registers*/
			
			/*
			if current > max current -> disable output, turn on led.
			if voltage > max voltage -> overvoltage  protection enabled.
			if any temperature > max temperature -> disable output.
			*/
		}
}
