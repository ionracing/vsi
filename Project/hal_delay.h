#ifndef HAL_DELAY_H
#define HAL_DELAY_H

/* Dependencies -------------------------------------------- */
#include "stdint.h"
#include "stm32f4xx_RCC.h"

/* MACROS ---------------------------------------------------*/
/*Constants -------------------------------------------------*/
/*Public functions ------------------------------------------*/
void hal_delay_systickInit(uint16_t frequency);
uint32_t hal_delay_getMillis(void);
void hal_delay_ms (uint32_t t);

#endif
