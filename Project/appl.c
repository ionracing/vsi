/*
* Main code for voltage source inverter, Fenrir 2017 - Aleksander K.F.
* Background task is run in main code. Regulator code is run in IRQ-handler
* controller by TIM1.
*/

#include "appl.h"
#include "HAL_GPIO.h"
#include "hardware.h"
#include "hal_delay.h"
#include "HAL_PWM.h"
#include "stm32f4xx_rcc.h"

enum 
{
    VSI_active,
    VSI_standby
}VSI_state_t;


#define SYSTICK_FREQ 1000 /* Hz */
volatile uint8_t state = VSI_standby;
int l = 0;
/* Local functions ---------------------------------------------*/
void active(void);
void standby(void);
void GPIO_INIT_OUT(portPin_t pxx);
uint8_t initCrystal(void);
enum
{
	orange,
	green,
	red,
	blue,
};

portPin_t ledDisc[] = 
{
	{GPIOD,GPIO_Pin_13},
	{GPIOD,GPIO_Pin_12},
	{GPIOD,GPIO_Pin_14},
	{GPIOD,GPIO_Pin_15},
};

#define CH_PWM TIM1
#define PIN_PWM_U GPIO_Pin_8
#define PORT_PWM_U GPIOA
#define PIN_NPWM_U GPIO_Pin_13
#define PORT_NPWM_U GPIOB

#define PIN_PWM_V GPIO_Pin_9
#define PORT_PWM_V GPIOA
#define PIN_NPWM_V GPIO_Pin_14
#define PORT_NPWM_V GPIOB

#define PIN_PWM_W GPIO_Pin_10
#define PORT_PWM_W GPIOA
#define PIN_NPWM_W GPIO_Pin_15
#define PORT_NPWM_W GPIOB


const portPin_t pwmGPIO[6] =
{
	{GPIOA, GPIO_Pin_8}, 	/* CH1 */
	{GPIOB, GPIO_Pin_13}, 	/* CH1N */
	{GPIOA, GPIO_Pin_9}, 	/* CH2 */
	{GPIOB, GPIO_Pin_14}, 	/* CH2N */
	{GPIOA, GPIO_Pin_10}, 	/* CH3 */
	{GPIOB, GPIO_Pin_15}, 	/* CH3N */
};

int main(void)
{
	
	hal_delay_systickInit(1000);

    
    GPIO_InitTypeDef GPIO_initStruct;
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
    GPIO_initStruct.GPIO_OType  = GPIO_OType_OD;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = GPIO_Pin_12; 
    GPIO_Init(GPIOB, &GPIO_initStruct);
	
	GPIO_ResetBits(GPIOB, GPIO_Pin_12);
	
    GPIO_INIT_OUT(led[led1]);
	
    while(1)
    {
        IO_HIGH(led[led1]);
        hal_delay_ms(500);
        IO_LOW(led[led1]);
        hal_delay_ms(500);
    }
}

void GPIO_INIT_OUT(portPin_t pxx)
{
	HAL_gpioEnableClock(pxx.port);
    GPIO_InitTypeDef GPIO_initStruct;
    GPIO_initStruct.GPIO_Mode   = GPIO_Mode_OUT;
    GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
    GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
    GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
    GPIO_initStruct.GPIO_Pin    = pxx.pin; 
    GPIO_Init(pxx.port, &GPIO_initStruct);
	
}

uint8_t initCrystal(void)
{
	RCC_HSEConfig(RCC_HSE_ON); /* Begin by starting up the HSE */
	/*TODO : Adjust timeout constant in stm32f4xx.h.*/
	if(RCC_WaitForHSEStartUp() == ERROR) 
	{
		/* Use internal crystal instead */
		RCC_HSEConfig(RCC_HSE_OFF);
		/*TODO: Enable internal crystal - wait for stabilization */
		/*TODO: configure clock multipliers for HSI operation */
		//RCC_PLLConfig
		/*TODO Report error ! */
	}else /*Crystal is running*/
	{
		/*Configure clock multipliers for HSE operation */
	}
	return 1;
}
