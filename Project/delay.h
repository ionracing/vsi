#ifndef DELAY_H
#define DELAY_H
/* Dependencies -------------------------------------------- */
#include "stdint.h"
#include "stm32f4xx_RCC.h"

/* MACROS ---------------------------------------------------*/
/*Constants -------------------------------------------------*/
/*Public functions ------------------------------------------*/
void systickInit(uint16_t frequency);
uint32_t get_millis(void);
void delay_ms (uint32_t t);
#endif

