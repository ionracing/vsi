#include "stdint.h"
/* Paralell interface*/

typedef union
{
	struct 
	{
		uint16_t ord0:1;
		uint16_t ord1:1;
		uint16_t ord2:1;
		uint16_t ord3:1;
		uint16_t ord4:1;
		uint16_t ord5:1;
		uint16_t ord6:1;
		uint16_t ord7:1;
		uint16_t ord8:1;
		uint16_t ord9:1;
		uint16_t ord10:1;
		uint16_t ord11:1;
	};
	struct 
	{
		uint16_t ord:12;
	};

}ord_t;

uint16_t hal_pgaReadGPIO(uint16_t data[13])
{
	ord_t val;
	val.ord0 = 
	val.ord1 = 
	val.ord2 = 
	val.ord3 = 
	val.ord4 = 
	val.ord5 = 
	val.ord6 = 
	val.ord7 = 
	val.ord8 = 
	val.ord9 = 
	val.ord10 =
	val.ord11 =
	return val.ord;
}

/*
Or this solution could be used :) A bit harder to understand, but does just the same.
*/

#define SET_BIT_N_TO_X(number,n,x) number ^= (-x ^ number) & (1 << n)
uint16_t hal_pgaReadGPIO(uint16_t data[13])
{
	ord_t val;
	for(int k = 0; k<12; k++)
	{
		SET_BIT_N_TO_X(val.ord, k, data[k]);
	}
	return val.ord;
}