#include "hal_gpio.h"

void HAL_gpioEnableClock(GPIO_TypeDef* GPIOx)
{
    if(GPIOx == GPIOA)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    }else if(GPIOx == GPIOB)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    }else if(GPIOx == GPIOC)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    }else if(GPIOx == GPIOD)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    }else if(GPIOx == GPIOE)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    }else if(GPIOx == GPIOF)
    {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
    }
}

uint16_t gpio_pins[16] = {1,2,4,8,16,32,64, 128, 256,512,1024,2048,4096,8192,16384,32768}; 

uint8_t HAL_gpioPin2pinSource(uint16_t gpioPin)
{
    for(uint8_t k = 0; k<16; k++)
    {
        if(gpio_pins[k] == gpioPin)
        {
            return k;
        }
    }
    return 16; /* GPIO_pins does not go as higher than 15. */
}
